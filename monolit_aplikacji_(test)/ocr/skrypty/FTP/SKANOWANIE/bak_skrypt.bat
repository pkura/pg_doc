@echo off

cd C:\FTP\SKANOWANIE
call C:\FTP\SKANOWANIE\skrypt_konwersja.bat

cd C:\FTP\SKANOWANIE
rem Dla Ka�dego katalogu zaczynajacego sie od K
for /D %%d in (K*) do cd %%d & call :insideFolder
@echo ...
@echo .......
@echo  zmiana nazw plikow ukonczona....
@echo ...
@echo .......
REM pause

.\skrypt_kopiowanie.bat

REM po wej�ciu w katalog
:insideFolder
REM nazwa folderu 
for %%* in (.) do set FOLDERNAME=%%~n*;
REM ustawienie prefiksu 
for /f "tokens=1 delims=_ " %%a in ("%FOLDERNAME%") do set PREF=%%a_
echo.PREFIX: %PREF%
REM set "PREF=%FOLDERNAME:~0,3%" 
call :files
call cd ..

REM dla wszystkich plik�w z rozszerzeniem dodaj prefix do nazwy
:files

for %%i in (*.txt *.tif *.tiff *.pdf) do  (set fName=%%i) & call :addPref

:addPref
@echo %fName%
rename %fName% %PREF%%fName%

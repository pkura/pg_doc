@echo off
set MAX=2
set /A "R = ((R+1) %% %MAX%)"

for %%v in (*.txt) do ren "%%v" "NODE_%R%_%%v"
for %%v in (*.tif) do ren "%%v" "NODE_%R%_%%v"
for %%v in (*.tiff) do ren "%%v" "NODE_%R%_%%v
for %%v in (*.png) do ren "%%v" "NODE_%R%_%%v""
for %%v in (*.pdf) do ren "%%v" "NODE_%R%_%%v"



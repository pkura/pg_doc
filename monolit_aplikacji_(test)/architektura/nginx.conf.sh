
#user  nobody;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;


events {
    worker_connections  1024;
}


http {

    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    open_file_cache max=1000 inactive=300s;
    open_file_cache_valid 360s;
    open_file_cache_min_uses 2;
    open_file_cache_errors off;

    #gzip  on;

    server {
        listen 443 ssl;
        server_name 153.19.62.37;

        ssl_certificate      ssl/server.crt;
        ssl_certificate_key  ssl/server.key;
        #ssl_session_cache    shared:SSL:1m;
        #ssl_session_timeout  5m;
        ssl_ciphers         HIGH:!aNULL:!MD5;
        ssl_prefer_server_ciphers  on;

        root /home/compan/docusafe-test/mobile/app;

        location /api {
            proxy_cookie_path /docusafe/ /;
            proxy_pass https://localhost:8443/docusafe/api;
        }

        location /ds {
            proxy_cookie_path /docusafe/ /;
            proxy_pass https://localhost:8443/docusafe;
            rewrite ^/ds/(.*) /docusafe/$1 break;
        }

        location /docusafe/ {
            proxy_cookie_path /docusafe/ /;
            proxy_pass https://localhost:8443;
            rewrite ^/(.*) /$1 break;
        }

        #location /dwrdockind {
        #    proxy_pass http://localhost:8080;
        #    rewrite ^/dwrdockind/(.*) /$1 break;
        #}

        location /docusafe/scripts/requirejs/dwr/dwr.js {

        }

        # DWR redirector for requirejs
        location /docusafe/scripts/requirejs/dwr {
            proxy_cookie_path /docusafe/ /;
            proxy_pass https://localhost:8443/docusafe;
            rewrite ^/scripts/requirejs/dwr/(.*) /docusafe/$1 break;
        }

                #location /scripts/configuration.js {
                #       try_files C:/app/mobile/configuration.js;
                #       #rewrite ^/scripts/configuration.js$ C:/app/mobile/configuration.js;
                #       #proxy_pass https://pg.docusafe.pl:8443/docusafe/api/is_logged_in;
                #}

        location /feeds/seznam/ {
                rewrite ^/feeds/seznam/$ /path/to/file/feed.xml;
        }

        location /docusafe/images {
            access_log off;
            add_header cache-control "private";
            expires 1y;
        }

        location @rewrite {
            rewrite ^/docusafe/(.*)$ /$1 break;
            #rewrite ^.*$ /index.html break;
        }

        location ~* ^/docusafe/(scripts|bower_components|styles|views|images|fonts)/.*$ {
            try_files $uri/ $uri @rewrite;
        }

        location /docusafe/ {
            try_files $uri/ $uri /index.html;
        }

        location ~* ^.+\.(html)$ {
            add_header cache-control "private, max-age=0, no-cache";
            expires -1;
        }

        location /docusafe/(scripts|styles) {
            access_log off;
            #add_header cache-control "private";
            #expires 1M;
            add_header cache-control "private, max-age=0, no-cache";
            expires -1;
        }
    }


    # HTTPS server
    #
    #server {
    #    listen       8092 ssl;
    #    server_name  localhost;
    #    ssl_certificate      ssl/server.crt;
    #    ssl_certificate_key  ssl/server.key;
    #    #ssl_session_cache    shared:SSL:1m;
    #    #ssl_session_timeout  5m;
    #    ssl_ciphers         HIGH:!aNULL:!MD5;
    #    ssl_prefer_server_ciphers  on;
    #    location / {
    #        proxy_pass http://localhost:80/;
    #    }
    #}

}

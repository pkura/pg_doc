#!/usr/bin/perl
use strict;
use warnings;

print "start!!!\n";
 
my $division = 'divisions.txt';
my $entity = 'entity_division.xml';
my $jcr_base_url = 'http://localhost:8083';
my @array = ("READ_ALL_DOCS", "administrators");
my $loopvariable; my $num = 0;

foreach $loopvariable (@array)
{
  upload ($loopvariable);
}

open(my $fh, '<:encoding(UTF-8)', $division)
  or die "Could not open file '$division' $!";
 
while (my $row = <$fh>) {
  chomp $row;
  upload ($row);
}

 
sub upload {
  my ($guid) = @_;
  $num += 1;
  $guid =~ s/^\s+|\s+$//g;
  open(my $fh1, '>:encoding(UTF-8)', $entity) 
	or die "Could not open file '$entity' $!";
	print $fh1 "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
	print $fh1 "<groupEntity>\n";
	print $fh1 "<guid>$guid</guid>\n";
	print $fh1 "<action>createOrUpdate</action>\n";
	print $fh1 "</groupEntity>\n";
  close $fh1;

  system "curl -u admin:admin -X PUT -d \@$entity -H \"Connection: Keep-Alive\" -H \"Content-Type: application/xml\" $jcr_base_url/jackrabbit/webapi/groups/ds";
  print " $num\n";
}

print "\ndone!!!\n";
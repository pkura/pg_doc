Synchronizacja struktury oraz użytkowaników w JCR z DS
Uwaga!!!
	W przypadku gdy zostało wyzerowane całe repozytorium, należy stworzyć noda "documents" w workspace attachment!!!
		New Node : {documents, typ:unstructured} => path = attachments/documents

(Kiedy występuje synchronizacja DS z systemem zewnętrznym np. MojaPG, należy wykonać potem synchronizację DS z JCR!)

Wymagania:
	Zainstalowany perl, curl, psql;

Postępowanie. Należy wykonać:


1)psql>
	Copy (SELECT guid FROM ds_division) To '<root_synch_jcr>\divisions.txt'
	Copy (SELECT externalname FROM ds_user) To 'C:\Users\Dell\Desktop\project_pg\architektura\jcr\users.txt'

2)W plikach
	- user_synch.pl,
	- division_synch.pl,
ustawić:
	- $jcr_base_url - url bazowy jackrabbita np. 'http://localhost:8083'
	(Uwaga na klastrach wystarczy wykoać synchronizacje na jedny node, one potem między sobą powinny się zsynchronizować)

3)Wykonać:
	a) perl division_synch.pl
	b) perl user_synch.pl

Ostatnie polecenia synchronizują repozytorimu.

Goood job!!!


Uwaga!
Jeżeli jest availablesach jackrabbit.users.externalName = true, to
	[SELECT externalname FROM ds_user] jest dobry. Natomiast jeżeli nie to
	należy wykonać:
	[SELECT name FROM ds_user]


Legenda
root_synch_jcr - katalog domowy synchronizatora jackrabbita
#!/usr/bin/perl
use strict;
use warnings;

print "start!!!\n";
 
my $users = 'users.txt';
my $entity = 'entity_user.xml';
my $jcr_base_url = 'http://localhost:8083';
my $num = 0;


open(my $fh, '<:encoding(UTF-8)', $users)
  or die "Could not open file '$users' $!";
 
while (my $row = <$fh>) {
  chomp $row;
  upload ($row);
}

sub upload {
  my ($name) = @_;
  $num += 1;
  $name =~ s/^\s+|\s+$//g;
  open(my $fh1, '>:encoding(UTF-8)', $entity) 
	or die "Could not open file '$entity' $!";
	print $fh1 "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
	print $fh1 "<userEntity>\n";
	print $fh1 "<username>$name</username>\n";
	print $fh1 "<password></password>\n";
	print $fh1 "</userEntity>\n";
  close $fh1;

  system "curl -u admin:admin -X POST -d \@$entity -H \"Connection: Keep-Alive\" -H \"Content-Type: application/xml\" $jcr_base_url/jackrabbit/webapi/users";
  print " $num\n";
}

print "\ndone!!!\n";
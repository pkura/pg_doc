(function(){
  angular.module('configuration', []).constant('CONFIG', {
    debug: true,
    text: {
      cannotConnectToBackend: "Aplikacja nie może połączyć się z serwerem Docusafe, ponawiam próbę ...",
      connectedToBackendSuccessfully: "Poprawnie połączono się z serwerem Docusafe :)",
      tasklist: {
        'in': "Pisma przychodzące",
        internal: "Pisma wewnętrzne",
        out: "Pisma wychodzące",
        cases: "Sprawy",
        watches: "Obserwowany",
        assignment: "Dekretuj zaznaczone",
        closeCase: "Zakończ zaznaczone sprawy",
        removeWatches: "Usuń zaznaczone obserwowane",
        empty: {
          'in': "Brak zadań",
          internal: "Brak zadań",
          out: "Brak zadań",
          cases: "Brak spraw",
          watches: "Brak obserwowanych"
        }
      },
      archivesearch: {
        folderSearch: "Wyszukiwanie teczek",
        caseSearch: "Wyszukiwanie spraw",
        documentSearch: "Wyszukiwanie dokumentów"
      },
      smartsearch: {
        error: {
          accessDenied: function(divisionName){
            return "Nie posiadasz uprawnień do tego dokumentu, co zostało odnotowane w systemie. Odnieś dokument do działu " + divisionName + ".";
          },
          locked: "Dokument zablokowany",
          notFound: "Nie znaleziono wyników",
          unknown: "Nieznany błąd: "
        }
      }
    },
    auth: {
      isPublicPath: function(path){
        return !!path.match(/\/(?:login|set-jsession)/);
      },
      cookieName: "JSESSIONID"
    },
    tasklist: {
      actions: {
        'in': ['assignment'],
        internal: ['assignment'],
        out: ['assignment'],
        cases: ['closeCase'],
        watches: ['removeWatches']
      }
    },
    connectionChecker: {
      enabled: false,
      timeout: 4000,
      interval: 4000
    },
    common: {
      prefixUrl: "/docusafe",
      getScannerUrl: function(qrCodeCallbackUrl){
        return "pic2shop://scan?callback=" + qrCodeCallbackUrl + "&barcode=QR_CODE";
      },
      spinnerDelay: 500,
      search: {
        limit: 50
      },
      loginPath: "/login",
      ssoLoginUrl: "http://localhost:8080/docusafe/service/cas-mobile-redirector.action",
      ssoLogoutUrl: "http://localhost:8080/docusafe/logout.do",
      startPath: "/tasklist/in",
      alertAutoCloseTimeout: 3500,
      casesEnabled: false
    },
    documentArchive: {
      getDocumentArchiveUrl: function(type){
        return "http://localhost:8080/docusafe/office/" + {
          'in': "incoming",
          out: "outgoing",
          internal: "internal"
        }[type] + "/document-archive.action";
      },
      dsRedirect: {
        "task-list": "/tasklist/in"
      }
    },
    backend: {
      connectionChecker: "api/ping",
      ping: "api/ping",
      login: "api/login2",
      logout: "api/logout2",
      is_logged_in: "api/is_logged_in",
      qr_decode: "api/qr_decode",
      search: {
        'default': "ds/repository/search-documents.action?jsonView=true",
        doSearch: "ds/repository/search-documents.action?jsonSearch=true",
        doSearchByIds: "api/document"
      },
      archivesearch: {
        'default': "ds/archives/find-archive.action",
        doSearch: "ds/archives/find-archive.action?doSearch=true"
      },
      tasklist: "ds/office/tasklist/user-task-list.action?jsonView=true",
      document: {
        'default': "api/document/{id}"
      },
      attachment: {
        'default': "api/attachment/{id}",
        byDocumentId: "api/attachment?documentId={id}",
        revision: {
          'default': "api/attachmentrevision/{id}/raw"
        }
      },
      documentDwr: {
        'default': "ds/repository/edit-dockind-document.action?jsonView=true",
        update: "ds/repository/edit-dockind-document.action?doUpdateDWR=true"
      },
      documentInProcess: {
        'default': "ds/office/incoming/document-archive.action?jsonView=true",
        processActions: {
          'in': "ds/office/incoming/document-archive.action?processActions=true",
          out: "ds/office/outgoing/document-archive.action?processActions=true",
          internal: "ds/office/internal/document-archive.action?processActions=true",
          watches: "ds/office/internal/document-archive.action?processActions=true",
          cases: "ds/office/internal/document-archive.action?processActions=true"
        },
        update: {
          'in': "ds/office/incoming/document-archive.action?doUpdateJson=true",
          out: "ds/office/outgoing/document-archive.action?doUpdateJson=true",
          internal: "ds/office/internal/document-archive.action?doUpdateJson=true",
          watches: "ds/office/internal/document-archive.action?doUpdateJson=true",
          cases: "ds/office/internal/document-archive.action?doUpdateJson=true"
        },
        process: {
          'in': "ds/office/incoming/document-archive.action?doProcessJsonAction=true",
          out: "ds/office/outgoing/document-archive.action?doProcessJsonAction=true",
          internal: "ds/office/internal/document-archive.action?doProcessJsonAction=true",
          watches: "ds/office/internal/document-archive.action?doProcessJsonAction=true",
          cases: "ds/office/internal/document-archive.action?doProcessJsonAction=true"
        }
      },
      smartsearch: {
        'default': "api/smartsearch",
        documentByExactBarcode: "api/smartsearch/document/barcode={barcode}",
        documentsByBarcodePrefix: "api/smartsearch/document/barcodePrefix={barcodePrefix}",
        document: "api/smartsearch/document",
        container: "api/smartsearch/container",
        getDocusafeDocumentUrl: function(documentId){
          return "http://localhost:8080/docusafe/repository/edit-dockind-document.action?id=" + documentId;
        }
      },
      division: "api/division",
      user: "api/user2",
      assignment: {
        'default': "ds/office/incoming/manual-multi-assignment.action?tab=in&doAssignJson=true",
        internal: "ds/office/internal/manual-multi-assignment.action?tab=internal&doAssignJson=true",
        out: "ds/office/outgoing/manual-multi-assignment.action?tab=out&doAssignJson=true",
        'in': "ds/office/incoming/manual-multi-assignment.action?tab=in&doAssignJson=true"
      }
    },
    spinner: {
      lines: 17,
      length: 40,
      width: 10,
      radius: 54,
      corners: 1,
      rotate: 0,
      direction: 1,
      color: '#000',
      speed: 1,
      trail: 60,
      shadow: false,
      hwaccel: false,
      className: 'spinner',
      zIndex: 2e9,
      top: '50%',
      left: '50%',
      position: 'fixed'
    }
  });
}).call(this);
